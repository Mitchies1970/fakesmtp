namespace FakeSmtp
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ModelEmails : DbContext
    {
        // Your context has been configured to use a 'ModelEmails' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'FakeSmtp.ModelEmails' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ModelEmails' 
        // connection string in the application configuration file.
        public ModelEmails()
            : base("name=ModelEmails")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Models.DBEmail> DbEmails { get; set; }
        public virtual DbSet<Models.DBAttachment> DbAttachments { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}