﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FakeSmtp.Models
{
    public class DBEmail
    {
        [Key]
        public Int64 Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public DateTime SentDate { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsBodyHtml { get; set; }
        public string Importance { get; set; }
        public List<DBAttachment> Attachments { get; set; }
    }

    public class DBAttachment
    {
        [Key]
        public Int64 AttachmentId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}